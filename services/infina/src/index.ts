import { GraphQLServer } from 'graphql-yoga';
import path from 'path';
import { growthRate, parseCSV, sortByCode } from './business';

const resolvers = {
  Query: {
    hello() {
      return "Hello Infina";
    }
  },
  Mutation: {
    stats() {
      const data = parseCSV(path.join(__dirname, './data.csv'));
      let growthRateList = data
        .map(d => growthRate(String(d[0]), Number(d[1]), Number(d[2])))
      growthRateList = growthRateList.sort(sortByCode);
      const max = growthRateList.reduce((prev, current) => (prev.growth > current.growth) ? prev : current)
      const min = growthRateList.reduce((prev, current) => (prev.growth < current.growth) ? prev : current)

      return { max, min, }
    },
  },
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
})

server.start(() => console.log('Server is running on http://localhost:8080'))

