import { Data, Stock } from "./type";
import fs from 'fs';

export function growthRate(code: string, lastPrice: number, previousClosed: number): Stock {
  return {
    code,
    growth: Number(((previousClosed - lastPrice) / previousClosed * 100).toFixed(2))
  }
}

export function sortByCode(a: Stock, b: Stock) {
  if (a.code < b.code) {
    return -1;
  }
  if (a.code > b.code) {
    return 1;
  }
  return 0;
}

export function parseCSV(pathFile: string) {
  let data = fs.readFileSync(pathFile, "utf8").split("\n").slice(1)
  const result: Data[][] = []
  for (let i in data) {
    result[i] = data[i].split(",")
  }

  return result
}  