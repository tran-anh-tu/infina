export type Data = string | number

export type Stock = {
  code: string;
  growth: number;
}